# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# epository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import bank
from . import party


def register():
    Pool.register(
        bank.Bank,
        bank.BankBSB,
        party.Party,
        module='account_aus', type_='model')
