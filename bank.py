# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.model import ModelView, ModelSQL

__all__ = ['Bank', 'BankBSB']


class Bank(metaclass=PoolMeta):
    __name__ = 'bank'
    bsb_codes = fields.One2Many('bank.bsb', 'bank', 'BSB Codes', required=True)


class BankBSB(ModelSQL, ModelView):
    'BankBSB'
    __name__ = 'bank.bsb'
    _rec_name = 'code'
    bank = fields.Many2One('bank', 'Bank', required=True)
    code = fields.Char('Code', required=True)
