# This file is part of Hotel module for Tryton.  The COPYRIGHT file at
# the top level of this repository contains the full copyright notices
# and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


__all__ = ['Party']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    id_number = fields.Char('Id Number')
    first_name = fields.Char('First Name')
    last_name = fields.Char('Last Name')
    bank_bsb = fields.Char('BSB')
    bank_account_number = fields.Char('Bank Account Number')
    type_document = fields.Selection([
            ('', ''),
            ('passport', 'Passport'),
            ('student_id', 'Student ID'),
            ('driver_license', 'Driver License'),
            ('medicare_card', 'Medicare Card'),
        ], 'Document Type')
